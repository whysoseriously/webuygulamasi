from flask import Blueprint,render_template, url_for, redirect, request, abort, jsonify
from .model import *
from .formlar import HaberEkle
from flask_login import login_required
mod_koseyazilaris = Blueprint('koseyazilarim',__name__)

from app import app

@mod_koseyazilaris.route('/')
@mod_koseyazilaris.route('/koseyazilar/<int:limit>/<int:offset>')
def koseyazilar(limit=9, offset=0):
    kosem=koseyazilari.query.limit(limit).offset(offset).all()
    kayitSayi = db.session.query(db.func.count(koseyazilari.yazino)).scalar()
    ilkyazilar = {
        'link1': url_for('koseyazilarim.koseyazilar', limit=9, offset=0)
    }
    oncekiyazilar = {
        'link1': url_for('koseyazilarim.koseyazilar', limit=9, offset=max(0, offset - limit))
    }
    sonrakiyazilar = {
        'link1': url_for('koseyazilarim.koseyazilar', limit=9, offset=min(kayitSayi, offset + limit))
    }
    sonyazilar = {
        'link1': url_for('koseyazilarim.koseyazilar', limit=9, offset=kayitSayi - 9)
    }
    return render_template('koseyazilari.html',title='Deneme',veri=kosem,
                           ilkYazi=ilkyazilar,
                           oncekiyazi=oncekiyazilar,
                           sonrayazi=sonrakiyazilar,
                           sonyazi=sonyazilar)

@mod_koseyazilaris.route('/koselistele/<int:limit>/<int:offset>')
@mod_koseyazilaris.route('/liste')
@login_required
def kyazilar(limit=9, offset=0):
    koselerim=koseyazilari.query.limit(limit).offset(offset).all()
    kayitSayi = db.session.query(db.func.count(koseyazilari.yazino)).scalar()
    ilkyazilar = {
        'link1': url_for('koseyazilarim.kyazilar', limit=9, offset=0)
    }
    oncekiyazilar = {
        'link1': url_for('koseyazilarim.kyazilar', limit=9, offset=max(0, offset - limit))
    }
    sonrakiyazilar = {
        'link1': url_for('koseyazilarim.kyazilar', limit=9, offset=min(kayitSayi, offset + limit))
    }
    sonyazilar = {
        'link1': url_for('koseyazilarim.kyazilar', limit=9, offset=kayitSayi - 9)
    }
    return render_template('Koselistele/koseliste.html', title='Deneme', veri1=koselerim,
                           ilkYazi=ilkyazilar,
                           oncekiyazi=oncekiyazilar,
                           sonrayazi=sonrakiyazilar,
                           sonyazi=sonyazilar
                           )

@mod_koseyazilaris.route('/ekle', methods=["GET", "POST"])
@login_required
def eklehaber():
    form = HaberEkle()
    if form.validate_on_submit():
        yeniHaber = koseyazilari()
        yeniHaber.yazino= form.koseid.data
        yeniHaber.yazibas = form.kosebaslik.data
        yeniHaber.yaziic = form.koseicerik.data
        yeniHaber.yazilink = form.koselinki.data
        yeniHaber.yaziresmi = form.koseresmi.data
        db.session.add(yeniHaber)
        db.session.commit()
        return redirect(url_for('koseyazilarim.kyazilar'))
    return render_template('Koselistele/koseekle.html', form=form,sayfa_baslik="Yeni Köşe Yazısı Ekle")

@mod_koseyazilaris.route("/sil", methods=['POST'])
@login_required
def habersil():
    haberNo = request.form["yazino"]

    silinecekHaber = koseyazilari.query.filter(koseyazilari.yazino == haberNo).one_or_none()

    if silinecekHaber is None:
        abort(404)

    db.session.delete(silinecekHaber)
    db.session.commit()

    return jsonify({'sonuc': 'TAMAM'})

@mod_koseyazilaris.route("/duzenle/<int:id>",methods=["GET", "POST"])
@login_required
def haberDuzenle(id):
   duzenlenecekHaber = koseyazilari.query.filter(koseyazilari.yazino == id).one_or_none()
   if duzenlenecekHaber is None:
       abort(404)
   form=HaberEkle()
   if form.validate_on_submit():
       #burası düüüzenleme tamamsa çalısacak.
       duzenlenecekHaber.yazino = form.koseid.data
       duzenlenecekHaber.yazibas = form.kosebaslik.data
       duzenlenecekHaber.yaziic = form.koseicerik.data
       duzenlenecekHaber.yazilink = form.koselinki.data
       duzenlenecekHaber.yaziresmi = form.koseresmi.data
       db.session.commit()
       return redirect(url_for('koseyazilarim.kyazilar'))
   form.koseid.data = duzenlenecekHaber.yazino
   form.kosebaslik.data = duzenlenecekHaber.yazibas
   form.koseicerik.data = duzenlenecekHaber.yaziic
   form.koselinki.data = duzenlenecekHaber.yazilink
   form.koseresmi.data = duzenlenecekHaber.yaziresmi
   # dolu formu gösterelim..
   return render_template('Koselistele/koseekle.html', form=form,sayfa_baslik="{} Haberini Düzenle".format(duzenlenecekHaber.yazibas))
