from flask_wtf import FlaskForm
from wtforms import  StringField, PasswordField,SubmitField,TextAreaField
from wtforms.validators import DataRequired
class HaberEkle(FlaskForm):
    koseid=StringField(label='Haber İd',validators=[DataRequired(message='Haber İd si Boş Geçilemez.')])
    kosebaslik=  StringField(label='Haber Başlığı',validators=[DataRequired(message='Haber Başlığı Boş Geçilemez.')])
    koseicerik= TextAreaField(label='Haber İçeriği',validators=[DataRequired(message='Haber İçeriği Bos Geçilemez')])
    koselinki = StringField(label='Haber Linki', validators=[DataRequired(message='Haber Linki Bos Geçilemez')])
    koseresmi = StringField(label='Haber  Resmi', validators=[DataRequired(message='Haber Resmi Bos Geçilemez')])
    ekle=SubmitField(label='Kaydet')




