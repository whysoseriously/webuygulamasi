from app import db

class haberlistesi(db.Model):
    no=db.Column(db.Integer,primary_key=True)
    haberId=db.Column(db.String(50))
    haberbas=db.Column(db.String(50))
    haberinici=db.Column(db.String(50))
    haberlinki=db.Column(db.String(50))
    haberresmi=db.Column(db.String(50))

class slayt(db.Model):
    slaytno = db.Column(db.Integer, primary_key=True)
    slaytlinki = db.Column(db.String(50))
    slaytfotosu = db.Column(db.String(255))
    slaytbas = db.Column(db.String(255))
    slaytinici = db.Column(db.String(255))
    slayttipi=db.Column(db.String(255))
    slaytId=db.Column(db.String(50))

