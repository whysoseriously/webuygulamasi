from flask import Blueprint, render_template, url_for, redirect, request, abort, jsonify
from .verimodeli import *
from .formlar import HaberEkle,SlaytEkle
from flask_login import login_required

mod_anasayfa = Blueprint('anasayfa', __name__)
mod_haberler = Blueprint('haberlistesis', __name__)
mod_slayt = Blueprint('slayts', __name__)
from app import app


@app.route('/')
@mod_haberler.route('/anasayfa/<int:limit>/<int:offset>')
@mod_haberler.route('/')
def anasayfa(limit=6, offset=0):
    anasayfaslayt = slayt.query.all()
    anasayfahaber = haberlistesi.query.limit(limit).offset(offset).all()
    kayitSayisi = db.session.query(db.func.count(haberlistesi.no)).scalar()
    ilkhaberler = {
        'link1': url_for('haberlistesis.anasayfa', limit=6, offset=0)
    }
    oncekihaberler = {
        'link1': url_for('haberlistesis.anasayfa', limit=6, offset=max(0, offset - limit))
    }
    sonrakihaberler = {
        'link1': url_for('haberlistesis.anasayfa', limit=6, offset=min(kayitSayisi, offset + limit))
    }
    sonhaberler = {
        'link1': url_for('haberlistesis.anasayfa', limit=6, offset=kayitSayisi-6  )
    }
    return render_template('mesajlar.html', title='Deneme',
                           veri=anasayfaslayt,
                           veri1=anasayfahaber,
                           ilkSayfa=ilkhaberler,
                           oncekihaber=oncekihaberler,
                           sonrahaber=sonrakihaberler,
                           sonhaber=sonhaberler)


@app.route('/hakkimizda')
def hakkimizda():
    hakkimizda = []
    return render_template('hakkimizda.html', title='Deneme', veri=hakkimizda)


@mod_haberler.route('/haberler/<int:limit>/<int:offset>')
@mod_haberler.route('/liste')
@login_required
def haberler(limit=6, offset=0):
    haberlerim = haberlistesi.query.limit(limit).offset(offset).all()
    kayitSayisi = db.session.query(db.func.count(haberlistesi.no)).scalar()
    ilkhaberler = {
        'link': url_for('haberlistesis.haberler', limit=6, offset=0)
    }
    oncekihaberler = {
        'link': url_for('haberlistesis.haberler', limit=6, offset=max(0, offset - limit))
    }
    sonrakihaberler = {
        'link': url_for('haberlistesis.haberler', limit=6, offset=min(kayitSayisi, offset + limit))
    }
    sonhaberler = {
        'link': url_for('haberlistesis.haberler', limit=6, offset=kayitSayisi-6)
    }

    return render_template('Haberlistele/anasayfam.html', veri=haberlerim,
                           ilkSayfa=ilkhaberler,
                           oncekihaber=oncekihaberler,
                           sonrahaber=sonrakihaberler,
                           sonhaber=sonhaberler
                           )


@mod_haberler.route('/ekle', methods=["GET", "POST"])
@login_required
def eklehaber():

    form = HaberEkle()
    if form.validate_on_submit():
        yeniHaber = haberlistesi()
        yeniHaber.haberId= form.haberid.data
        yeniHaber.haberbas = form.haberbaslik.data
        yeniHaber.haberinici = form.habericerik.data
        yeniHaber.haberlinki = form.haberlinki.data
        yeniHaber.haberresmi = form.haberresmi.data
        db.session.add(yeniHaber)
        db.session.commit()
        return redirect(url_for('haberlistesis.haberler'))
    return render_template('Haberlistele/haberekle.html', form=form,sayfa_baslik="Yeni Haber Ekle")


@mod_haberler.route("/sil", methods=['POST'])
@login_required
def habersil():
    haberNo = request.form["no"]

    silinecekHaber = haberlistesi.query.filter(haberlistesi.no == haberNo).one_or_none()

    if silinecekHaber is None:
        abort(404)

    db.session.delete(silinecekHaber)
    db.session.commit()

    return jsonify({'sonuc': 'TAMAM'})

@mod_haberler.route("/duzenle/<int:id>",methods=["GET", "POST"])
@login_required
def haberDuzenle(id):
   duzenlenecekHaber = haberlistesi.query.filter(haberlistesi.no == id).one_or_none()
   if duzenlenecekHaber is None:
       abort(404)
   form=HaberEkle()
   if form.validate_on_submit():
       #burası düüüzenleme tamamsa çalısacak.
       duzenlenecekHaber.haberId = form.haberid.data
       duzenlenecekHaber.haberbas = form.haberbaslik.data
       duzenlenecekHaber.haberinici = form.habericerik.data
       duzenlenecekHaber.haberlinki = form.haberlinki.data
       duzenlenecekHaber.haberresmi = form.haberresmi.data
       db.session.commit()
       return redirect(url_for('haberlistesis.haberler'))
   form.haberid.data = duzenlenecekHaber.haberId
   form.haberbaslik.data = duzenlenecekHaber.haberbas
   form.habericerik.data = duzenlenecekHaber.haberinici
   form.haberlinki.data = duzenlenecekHaber.haberlinki
   form.haberresmi.data = duzenlenecekHaber.haberresmi
   # dolu formu gösterelim..
   return render_template('Haberlistele/haberekle.html', form=form,sayfa_baslik="{} Haberini Düzenle".format(duzenlenecekHaber.haberbas))




@mod_slayt.route('/slaytlar/<int:limit>/<int:offset>')
@mod_slayt.route('/liste')
@login_required
def slaytlar(limit=6, offset=0):
    slaytlarim = slayt.query.limit(limit).offset(offset).all()
    kayitSayisi = db.session.query(db.func.count(slayt.slaytno)).scalar()

    ilkslayt = {
        'link1': url_for('slayts.slaytlar', limit=6, offset=0)
    }
    oncekislayt= {
        'link1': url_for('slayts.slaytlar', limit=6, offset=max(0, offset - limit))
    }
    sonrakislayt = {
        'link1': url_for('slayts.slaytlar', limit=6, offset=min(kayitSayisi, offset + limit))
    }
    sonslayt = {
        'link1': url_for('slayts.slaytlar', limit=6, offset=kayitSayisi-6)
    }

    return render_template('Slaytlistele/slaytListe.html',
                           veri=slaytlarim,
                           ilkSayfa=ilkslayt,
                           oncekihaber=oncekislayt,
                           sonrahaber=sonrakislayt,
                           sonhaber=sonslayt
                           )

@mod_slayt.route('/ekle', methods=["GET", "POST"])
@login_required
def ekleslayt():
    form = SlaytEkle()
    if form.validate_on_submit():
        yeniSlayt= slayt()
        yeniSlayt.slaytId= form.slaytid.data
        yeniSlayt.slaytbas = form.slaytbaslik.data
        yeniSlayt.slaytinici = form.slayticerik.data
        yeniSlayt.slaytlinki = form.slaytlinki.data
        yeniSlayt.slaytfotosu = form.slaytresmi.data
        db.session.add(yeniSlayt)
        db.session.commit()
        return redirect(url_for('slayts.slaytlar'))
    return render_template('Slaytlistele/slaytekle.html', form=form,sayfa_baslik="Yeni Slayt Ekle")

@mod_slayt.route("/sil", methods=['POST'])
@login_required
def slaytsil():
    slaytNo = request.form["slaytno"]

    silinecekSlayt = slayt.query.filter(slayt.slaytno == slaytNo).one_or_none()

    if silinecekSlayt is None:
        abort(404)

    db.session.delete(silinecekSlayt)
    db.session.commit()

    return jsonify({'sonuc': 'TAMAM'})

@mod_slayt.route("/duzenle/<int:id>",methods=["GET", "POST"])
@login_required
def slaytDuzenle(id):
   duzenlenecekSlayt = slayt.query.filter(slayt.slaytno == id).one_or_none()
   if duzenlenecekSlayt is None:
       abort(404)
   form=SlaytEkle()
   if form.validate_on_submit():
       #burası düüüzenleme tamamsa çalısacak.
       duzenlenecekSlayt.slaytId = form.slaytid.data
       duzenlenecekSlayt.slaytbas = form.slaytbaslik.data
       duzenlenecekSlayt.slaytinici = form.slayticerik.data
       duzenlenecekSlayt.slaytlinki = form.slaytlinki.data
       duzenlenecekSlayt.slaytfotosu = form.slaytresmi.data
       db.session.commit()
       return redirect(url_for('slayts.slaytlar'))
   form.slaytid.data = duzenlenecekSlayt.slaytId
   form.slaytbaslik.data = duzenlenecekSlayt.slaytbas
   form.slayticerik.data = duzenlenecekSlayt.slaytinici
   form.slaytlinki.data = duzenlenecekSlayt.slaytlinki
   form.slaytresmi.data = duzenlenecekSlayt.slaytfotosu
   # dolu formu gösterelim..
   return render_template('Slaytlistele/slaytekle.html', form=form,sayfa_baslik="{} Slaytını Düzenle".format(duzenlenecekSlayt.slaytbas))
