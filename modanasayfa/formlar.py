from flask_wtf import FlaskForm
from wtforms import  StringField, PasswordField,SubmitField,TextAreaField
from wtforms.validators import DataRequired
class HaberEkle(FlaskForm):
    haberid=StringField(label='Haber İd',validators=[DataRequired(message='Haber İd si Boş Geçilemez.')])
    haberbaslik=  StringField(label='Haber Başlığı',validators=[DataRequired(message='Haber Başlığı Boş Geçilemez.')])
    habericerik= TextAreaField(label='Haber İçeriği',validators=[DataRequired(message='Haber İçeriği Bos Geçilemez')])
    haberlinki = StringField(label='Haber Linki', validators=[DataRequired(message='Haber Linki Bos Geçilemez')])
    haberresmi = StringField(label='Haber  Resmi', validators=[DataRequired(message='Haber Resmi Bos Geçilemez')])
    ekle=SubmitField(label='Kaydet')


class SlaytEkle(FlaskForm):
    slaytid=StringField(label='Slayt İd',validators=[DataRequired(message='Slayt İd si Boş Geçilemez.')])
    slaytbaslik=  StringField(label='Slayt Başlığı',validators=[DataRequired(message='Slayt Başlığı Boş Geçilemez.')])
    slayticerik= TextAreaField(label='Slayt İçeriği',validators=[DataRequired(message='Slayt İçeriği Bos Geçilemez')])
    slaytlinki = StringField(label='Slayt Linki', validators=[DataRequired(message='Slayt Linki Bos Geçilemez')])
    slaytresmi = StringField(label='Slayt  Resmi', validators=[DataRequired(message='Slayt Resmi Bos Geçilemez')])
    ekle=SubmitField(label='Kaydet')



