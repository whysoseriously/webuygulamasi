from flask import Flask,render_template

from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_wtf.csrf import CSRFProtect
from flask_login import LoginManager
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///teknonet.db'
app.config['SECRET_KEY']='ÇOK GİZLİ BİLGİ'
csrf=CSRFProtect(app)
db=SQLAlchemy(app)
login=LoginManager(app)
login.login_view="kullanici.kullaniciLogin"


from mod_kullanici.yonetici import mod_kullanici
from modanasayfa.yonetici import mod_haberler
from modanasayfa.yonetici import mod_slayt
from mod_koseyazilari.yonetici import mod_koseyazilaris
from mod_sondakika.yonetici import mod_sonhaberler
from mod_sondakika.yonetici import mod_sonslayt


migrate=Migrate(app,db)


import modanasayfa.yonetici
import mod_iletisimformu.yonetici
import mod_sondakika.yonetici
import mod_koseyazilari.yonetici
import mod_iletisimegec.yonetici
import mod_sondakika.yonetici
@app.route('/ertan')
def index():
    return render_template('hakkimizda.html')
app.register_blueprint(mod_kullanici, url_prefix='/giris')
app.register_blueprint(mod_haberler, url_prefix='/haber')
app.register_blueprint(mod_slayt, url_prefix='/slayt')
app.register_blueprint(mod_koseyazilaris,url_prefix='/kose')
app.register_blueprint(mod_sonhaberler,url_prefix='/sondakika')
app.register_blueprint(mod_sonslayt,url_prefix='/sondakikaslayt')






if __name__ == '__main__':
    app.run()
