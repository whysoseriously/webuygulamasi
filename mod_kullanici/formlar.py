from flask_wtf import FlaskForm
from wtforms import  StringField, PasswordField,SubmitField,BooleanField
from wtforms.validators import DataRequired
class LoginFormu(FlaskForm):
    uyeNo=  StringField(label='Kullanıcı Adınız',validators=[DataRequired(message='Kullanıcı Adı Boş Geçilemez.')])
    sifre= PasswordField(label='Şifreniz',validators=[DataRequired(message='Şifre Boş Geçilemez.')])
    beniHatirla=BooleanField(label='Beni Hatırla')
    gonder=SubmitField(label='Giris')
