from flask_wtf import FlaskForm
from wtforms import  StringField, PasswordField,SubmitField,TextAreaField
from wtforms.validators import DataRequired
class HaberEkle(FlaskForm):
    sondakikaid=StringField(label='Haber İd',validators=[DataRequired(message='Haber İd si Boş Geçilemez.')])
    sondakikabaslik=  StringField(label='Haber Başlığı',validators=[DataRequired(message='Haber Başlığı Boş Geçilemez.')])
    sondakikaicerik= TextAreaField(label='Haber İçeriği',validators=[DataRequired(message='Haber İçeriği Bos Geçilemez')])
    sondakikalinki = StringField(label='Haber Linki', validators=[DataRequired(message='Haber Linki Bos Geçilemez')])
    sondakikaresmi = StringField(label='Haber  Resmi', validators=[DataRequired(message='Haber Resmi Bos Geçilemez')])
    ekle=SubmitField(label='Kaydet')


class SlaytEkle(FlaskForm):
    sondakikaslaytid=StringField(label='Slayt İd',validators=[DataRequired(message='Slayt İd si Boş Geçilemez.')])
    sondakikaslaytbaslik=  StringField(label='Slayt Başlığı',validators=[DataRequired(message='Slayt Başlığı Boş Geçilemez.')])
    sondakikaslayticerik= TextAreaField(label='Slayt İçeriği',validators=[DataRequired(message='Slayt İçeriği Bos Geçilemez')])
    sondakikaslaytlinki = StringField(label='Slayt Linki', validators=[DataRequired(message='Slayt Linki Bos Geçilemez')])
    sondakikaslaytresmi = StringField(label='Slayt  Resmi', validators=[DataRequired(message='Slayt Resmi Bos Geçilemez')])
    ekle=SubmitField(label='Kaydet')



