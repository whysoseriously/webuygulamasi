from flask import Blueprint,render_template, url_for, redirect, request, abort, jsonify
from .model import *
from .formlar import HaberEkle,SlaytEkle
from flask_login import login_required
mod_sonhaberler = Blueprint('sondakikahaber',__name__)
mod_sonslayt=Blueprint('sonslayt',__name__)
from app import app


@mod_sonhaberler.route('/')
@mod_sonhaberler.route('/sondakikalar/<int:limit>/<int:offset>')
def sondakikalar(limit=6, offset=0):
    sonslayt = sondakikaslayt.query.all()
    sondakikam = sondakika.query.limit(limit).offset(offset).all()
    kayitSayisi = db.session.query(db.func.count(sondakika.sonno)).scalar()
    ilkhaber = {
        'link1': url_for('sondakikahaber.sondakikalar', limit=6, offset=0)
    }
    oncekihaber= {
        'link1': url_for('sondakikahaber.sondakikalar', limit=6, offset=max(0, offset - limit))
    }
    sonrakihaber = {
        'link1': url_for('sondakikahaber.sondakikalar', limit=6, offset=min(kayitSayisi, offset + limit))
    }
    sonhaber = {
        'link1': url_for('sondakikahaber.sondakikalar', limit=6, offset=kayitSayisi-6)
    }

    return render_template('sondakika.html',title='Deneme',veri=sondakikam,veri1=sonslayt,
                           ilkSayfa=ilkhaber,
                           oncekihaber=oncekihaber,
                           sonrahaber=sonrakihaber,
                           sonhaber=sonhaber
                           )

@mod_sonhaberler.route('/sondakikalistele/<int:limit>/<int:offset>')
@mod_sonhaberler.route('/liste')
@login_required
def kyazilar(limit=9, offset=0):
    koselerim=sondakika.query.limit(limit).offset(offset).all()
    kayitSayi = db.session.query(db.func.count(sondakika.sonno)).scalar()
    ilkyazilar = {
        'link1': url_for('sondakikahaber.kyazilar', limit=9, offset=0)
    }
    oncekiyazilar = {
        'link1': url_for('sondakikahaber.kyazilar', limit=9, offset=max(0, offset - limit))
    }
    sonrakiyazilar = {
        'link1': url_for('sondakikahaber.kyazilar', limit=9, offset=min(kayitSayi, offset + limit))
    }
    sonyazilar = {
        'link1': url_for('sondakikahaber.kyazilar', limit=9, offset=kayitSayi - 9)
    }
    return render_template('Sondakikalistele/sonliste.html', title='Deneme', veri=koselerim,
                           ilkYazi=ilkyazilar,
                           oncekiyazi=oncekiyazilar,
                           sonrayazi=sonrakiyazilar,
                           sonyazi=sonyazilar
                           )

@mod_sonslayt.route('/sondakikaslaytlistele/<int:limit>/<int:offset>')
@mod_sonslayt.route('/liste')
@login_required
def syazilar(limit=9, offset=0):
    slaytlarimmm=sondakikaslayt.query.limit(limit).offset(offset).all()
    kayitSayi = db.session.query(db.func.count(sondakikaslayt.slaytno)).scalar()
    ilkyazilar = {
        'link1': url_for('sonslayt.syazilar', limit=9, offset=0)
    }
    oncekiyazilar = {
        'link1': url_for('sonslayt.syazilar', limit=9, offset=max(0, offset - limit))
    }
    sonrakiyazilar = {
        'link1': url_for('sonslayt.syazilar', limit=9, offset=min(kayitSayi, offset + limit))
    }
    sonyazilar = {
        'link1': url_for('sonslayt.syazilar', limit=9, offset=kayitSayi -1)
    }
    return render_template('SonslaytListele/sonslaytListe.html', title='Deneme', veri=slaytlarimmm,
                           ilkYazi=ilkyazilar,
                           oncekiyazi=oncekiyazilar,
                           sonrayazi=sonrakiyazilar,
                           sonyazi=sonyazilar
                           )

@mod_sonhaberler.route('/ekle', methods=["GET", "POST"])
@login_required
def eklehaber():
    form = HaberEkle()
    if form.validate_on_submit():
        yeniHaber = sondakika()
        yeniHaber.sonno= form.sondakikaid.data
        yeniHaber.sonbas = form.sondakikabaslik.data
        yeniHaber.sonic = form.sondakikaicerik.data
        yeniHaber.sonlink = form.sondakikalinki.data
        yeniHaber.sonresim = form.sondakikaresmi.data
        db.session.add(yeniHaber)
        db.session.commit()
        return redirect(url_for('sondakikahaber.kyazilar'))
    return render_template('Sondakikalistele/sondakikaekle.html', form=form,sayfa_baslik="Yeni Son Dakika Ekle")

@mod_sonslayt.route('/ekle', methods=["GET", "POST"])
@login_required
def ekleslayt():
    form = SlaytEkle()
    if form.validate_on_submit():
        yeniSlayt= sondakikaslayt()
        yeniSlayt.slaytno= form.sondakikaslaytid.data
        yeniSlayt.slaytbas = form.sondakikaslaytbaslik.data
        yeniSlayt.slaytic = form.sondakikaslayticerik.data
        yeniSlayt.slaytlink = form.sondakikaslaytlinki.data
        yeniSlayt.slaytresim = form.sondakikaslaytresmi.data
        db.session.add(yeniSlayt)
        db.session.commit()
        return redirect(url_for('sonslayt.syazilar'))
    return render_template('SonslaytListele/sonslaytekle.html', form=form,sayfa_baslik="Yeni Son Dakika Slayt Ekle")


@mod_sonhaberler.route("/sil", methods=['POST'])
@login_required
def habersil():
    haberNo = request.form["sonno"]

    silinecekHaber = sondakika.query.filter(sondakika.sonno == haberNo).one_or_none()

    if silinecekHaber is None:
        abort(404)

    db.session.delete(silinecekHaber)
    db.session.commit()

    return jsonify({'sonuc': 'TAMAM'})


@mod_sonslayt.route("/sil", methods=['POST'])
@login_required
def slaytsil():
    haberNo = request.form["slaytno"]

    silinecekHaber = sondakikaslayt.query.filter(sondakikaslayt.slaytno == haberNo).one_or_none()

    if silinecekHaber is None:
        abort(404)

    db.session.delete(silinecekHaber)
    db.session.commit()

    return jsonify({'sonuc': 'TAMAM'})

@mod_sonhaberler.route("/duzenle/<int:id>",methods=["GET", "POST"])
@login_required
def haberDuzenle(id):
   duzenlenecekHaber = sondakika.query.filter(sondakika.sonno == id).one_or_none()
   if duzenlenecekHaber is None:
       abort(404)
   form=HaberEkle()
   if form.validate_on_submit():
       #burası düüüzenleme tamamsa çalısacak.
       duzenlenecekHaber.sonno = form.sondakikaid.data
       duzenlenecekHaber.sonbas = form.sondakikabaslik.data
       duzenlenecekHaber.sonic = form.sondakikaicerik.data
       duzenlenecekHaber.sonlink = form.sondakikalinki.data
       duzenlenecekHaber.sonresim = form.sondakikaresmi.data
       db.session.commit()
       return redirect(url_for('sondakikahaber.kyazilar'))
   form.sondakikaid.data = duzenlenecekHaber.sonno
   form.sondakikabaslik.data = duzenlenecekHaber.sonbas
   form.sondakikaicerik.data = duzenlenecekHaber.sonic
   form.sondakikalinki.data = duzenlenecekHaber.sonlink
   form.sondakikaresmi.data = duzenlenecekHaber.sonresim
   # dolu formu gösterelim..
   return render_template('Sondakikalistele/sondakikaekle.html', form=form,sayfa_baslik="{} Haberini Düzenle".format(duzenlenecekHaber.sonbas))




@mod_sonslayt.route("/duzenle/<int:id>",methods=["GET", "POST"])
@login_required
def slaytDuzenle(id):
   duzenlenecekHaber = sondakikaslayt.query.filter(sondakikaslayt.slaytno == id).one_or_none()
   if duzenlenecekHaber is None:
       abort(404)
   form=SlaytEkle()
   if form.validate_on_submit():
       #burası düüüzenleme tamamsa çalısacak.
       duzenlenecekHaber.slaytno = form.sondakikaslaytid.data
       duzenlenecekHaber.slaytbas = form.sondakikaslaytbaslik.data
       duzenlenecekHaber.slaytic = form.sondakikaslayticerik.data
       duzenlenecekHaber.slaytlink = form.sondakikaslaytlinki.data
       duzenlenecekHaber.slaytresim = form.sondakikaslaytresmi.data
       db.session.commit()
       return redirect(url_for('sonslayt.syazilar'))
   form.sondakikaslaytid.data = duzenlenecekHaber.slaytno
   form.sondakikaslaytbaslik.data = duzenlenecekHaber.slaytbas
   form.sondakikaslayticerik.data = duzenlenecekHaber.slaytic
   form.sondakikaslaytlinki.data = duzenlenecekHaber.slaytlink
   form.sondakikaslaytresmi.data = duzenlenecekHaber.slaytresim
   # dolu formu gösterelim..
   return render_template('SonslaytListele/sonslaytekle.html', form=form,sayfa_baslik="{} Haberini Düzenle".format(duzenlenecekHaber.slaytbas))

