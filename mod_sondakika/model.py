from app import db


class sondakika(db.Model):
    sonno=db.Column(db.Integer,primary_key=True)
    sonbas=db.Column(db.String(50))
    sonic=db.Column(db.String(255))
    sonlink=db.Column(db.String(255))
    sonresim=db.Column(db.String(255))

class sondakikaslayt(db.Model):
    slaytno = db.Column(db.Integer, primary_key=True)
    slaytbas = db.Column(db.String(50))
    slaytic = db.Column(db.String(255))
    slaytlink = db.Column(db.String(255))
    slaytresim = db.Column(db.String(255))
    slayttipi=db.Column(db.String(255))